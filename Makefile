BOARD_TAG    = uno

ARDUINO_LIBS  = RGBdriver Timer LiquidCrystal Pitches Event

USER_LIB_PATH = $(realpath ./lib)

include /usr/share/arduino/Arduino.mk
