/*                                                                                                                                                                                                
 *
 *      Copyright 2017-2018 Ruben Rodriguez <ruben@gnu.org>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */


#include <LiquidCrystal.h>
#include <pitches.h>
#include <RGBdriver.h>
#include <Timer.h>
#define CLK 2
#define DIO 3


Timer t;

RGBdriver Driver(CLK,DIO);


 
// select the pins used on the LCD panel
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

// define some values used by the panel and buttons
int lcd_key     = 0;
int adc_key_in  = 0;
int button  = 0;
#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5

#define modeFOCUS   0
#define modeTEST    1
#define modePRINT   2
#define modeTSPLIT1  3
#define modeTSPLIT2 4
#define modePRINTS  5

int countdown;
int led = 0;
int sensorValue = 0;
int sensorPin = A1;
int timer=0;
int timer5=1;

int mode = modeFOCUS;
bool lighton = false;
bool beeperon = false;


void lightset(int r,int g,int b){
  Driver.begin(); 
  Driver.SetColor(g,r,b);
  Driver.end();

}

void lightoff(){
  lightset(0,0,0);
  lighton = false;
  tone(13, NOTE_C4,200);
}

void reset(int m){
  lcd.clear();
  lightset(0,0,0);
  lighton=false;
  mode=m;
  beeperon=false;
}

int read_LCD_buttons()
{
  //button = digitalRead(12);
  //if (button){
   // develop();
  //}

  adc_key_in = analogRead(0);      // read the value from the sensor 
  //delay(50); //switch debounce delay. Increase this delay if incorrect switch selections are returned.
  //int k = (analogRead(0) - adc_key_in); //gives the button a slight range to allow for a little contact resistance noise
  //if (5 < abs(k)) return btnNONE;  // double checks the keypress. If the two readings are not equal +/-k value after debounce delay, it tries again.
  // my buttons when read are centered at these valies: 0, 144, 329, 504, 741
  // we add approx 50 to those values and check to see if we are close
  if (adc_key_in > 1000) return btnNONE; // We make this the 1st option for speed reasons since it will be the most likely result
  delay(300);
  if (adc_key_in < 50)   return btnRIGHT;  
  if (adc_key_in < 250)  return btnUP; 
  if (adc_key_in < 450)  return btnDOWN; 
  if (adc_key_in < 650)  return btnLEFT; 
  if (adc_key_in < 850)  return btnSELECT; 
  return btnNONE;  // when all others fail, return this...
}

void develop(){
  reset(mode);
  lcd.setCursor(0,0);
  lcd.print("Developer ready?");
  tone(13, NOTE_C4,200);
  delay(5000);
  tone(13, NOTE_C5,200);

  for  (countdown=60; countdown>0; countdown--){
   if (read_LCD_buttons() == btnSELECT ){
     reset(mode);
     delay(1000);
     return;
   }
   lcd.setCursor(0,0);
   lcd.clear();
   lcd.print(String("Developer: " + String(countdown)));
   if (countdown == 10) tone(13, NOTE_A3,200);
   delay(1000);
  }
  tone(13, NOTE_C4,200);

 for  (countdown=20; countdown>0; countdown--){
   if (read_LCD_buttons() == btnSELECT ){
     reset(mode);
     delay(1000);
     return;
   }
   lcd.setCursor(0,0);
   lcd.clear();
   lcd.print(String("Stop bath: " + String(countdown)));
   if (countdown == 10) tone(13, NOTE_A3,200);
   delay(1000);
  }
  tone(13, NOTE_C4,200);

 for  (countdown=60; countdown>0; countdown--){
   if (read_LCD_buttons() == btnSELECT ){
     reset(mode);
     delay(1000);
     return;
   }
   lcd.setCursor(0,0);
   lcd.clear();
   lcd.print(String("Rapid fix: " + String(countdown)));
   if (countdown == 40) tone(13, NOTE_A3,200);
   delay(1000);
  }
  tone(13, NOTE_C4,100);
  delay(500);
  tone(13, NOTE_C4,100);
  delay(500);
  tone(13, NOTE_C4,100);
  lcd.clear();
}


void setup(){
 lcd.begin(16, 2); 
  reset(0);
  tone(13, NOTE_C5,300);
}

void beep(){
  if (beeperon)
    if (timer5 == 5){
      timer5=1;
      tone(13, NOTE_C4,50);
    }else {
      timer5=timer5+1;
      tone(13, NOTE_C3,50);
    }
  beeperon=false;
}

void loop(){
  t.update();
  switch (mode) {
    case modeFOCUS: {
      lcd.setCursor(0,0);
      lcd.print("Focus");
      lcd_key = read_LCD_buttons();
      switch (lcd_key){
        case btnSELECT: {
          if (lighton){
            lcd.setCursor(0,1);
            lightset(0,0,0);
            lcd.print("light off       ");
            lighton=false;
          }else{            
            lcd.setCursor(0,1);
            lightset(60,130,30);
            lighton=true;
            lcd.print("light on medium ");
          }
          break;
        }
        case btnLEFT: {
          lcd.setCursor(0,1);
          lightset(30,65,15);
          lighton=true;
          lcd.print("light on low     ");
          break;
        }
        case btnRIGHT: {
          lcd.setCursor(0,1);
          lightset(120,255,60);
          lighton=true;
          lcd.print("light on high    ");
          break;
        }
        case btnUP: {
          reset(modeTSPLIT2);
          break;
        } 
        case btnDOWN: {
          reset(mode + 1);
          break;
        } 
      }
      break;
    }

    case modeTEST: {
      lcd.setCursor(0,0);
      lcd.print("Test");
      lcd_key = read_LCD_buttons();
      if (lighton && ! beeperon){
        t.after(1000, beep);
        beeperon=true;
      }
      switch (lcd_key){
        case btnSELECT: {
          if (lighton){
            lcd.setCursor(0,1);
            lightset(0,0,0);
            lcd.print("light off       ");
            timer5=1;
            beeperon=false;
            lighton=false;
          }else{
            lcd.setCursor(0,1);
            lcd.print("Ready?           ");
            delay(1000);
            beeperon=true;
            beep();
            lcd.setCursor(0,1);
            lightset(120,65,20);
            lcd.print("light on        ");
            lighton=true;
            if (! beeperon){
              t.after(1000, beep);
              beeperon=true;
            }
         }
          break;
        }
        case btnLEFT: {
          lightset(150,0,0);
          lcd.setCursor(0,1);
          lcd.print("safelight on    ");
          break;
        }
        case btnUP: {
          reset(mode - 1);
          break;
        } 
        case btnDOWN: {
          reset(mode + 1);
          break;
        } 
      }
      break;
    }


    case modePRINT: {
      lcd.setCursor(0,0);
      lcd.print("Print");
      lcd.setCursor(0,1);
      lcd.print("Time: ");
      lcd.print(timer);
      lcd_key = read_LCD_buttons();
      if (! lighton){
        lcd.setCursor(10,1);
        lcd.print("OFF");
      }
      switch (lcd_key){
        case btnSELECT: {
          if (lighton){
            lcd.setCursor(10,1);
            lcd.print("OFF    ");
            lightset(0,0,0);
            lighton=false;
          }else{
            if (timer > 0){
              lcd.setCursor(10,1);
              lcd.print("Ready?");
              tone(13, NOTE_C4,200);
              delay(1000);
              tone(13, NOTE_C4,200);
              lcd.setCursor(10,1);
              lcd.print("ON    ");
              lightset(120,65,20);
              lighton=true;
              t.after(timer * 1000, lightoff);
              t.after((timer + 2) * 1000, develop);
            }
          }
          break;
        }
        case btnLEFT: {
          timer = timer + 5;
          lcd.setCursor(0,1);
          lcd.print("Time:     ");
          lcd.setCursor(6,1);
          lcd.print(timer);
          break;
        }
        case btnRIGHT: {
          timer = timer + 1 ;
          lcd.setCursor(0,1);
          lcd.print("Time:     ");
          lcd.setCursor(6,1);
          lcd.print(timer);
          break;
        }
        case btnUP: {
          reset(mode - 1);
          break;
        } 
        case btnDOWN: {
          reset(mode + 1);
          break;
        } 
      }
      break;
    }

    case modeTSPLIT1: {
      lcd.setCursor(0,0);
      lcd.print("Test Lo Contrast");
      lcd_key = read_LCD_buttons();
      if (lighton && ! beeperon){
        t.after(1000, beep);
        beeperon=true;
      }
      switch (lcd_key){ 
        case btnSELECT: {
          if (lighton){
            lcd.setCursor(0,1);
            lightset(0,0,0);
            lcd.print("light off       ");
            beeperon=false;
            lighton=false;
          }else{
            lcd.setCursor(0,1);
            lcd.print("Ready?           ");
            delay(1000);
            beeperon=true;
            beep();
            lcd.setCursor(0,1);
            lightset(0,200,0);
            lcd.print("light on        ");
            lighton=true;
            if (! beeperon){
              t.every(1000, beep, 1);
              beeperon=true;
            }
         }
          break;
        }
        case btnLEFT: {
          lightset(150,0,0);
          lcd.setCursor(0,1);                                                                    
          lcd.print("safelight on    ");
          break;
        }
        case btnUP: {
          reset(mode - 1);
          break;
        } 
        case btnDOWN: {
          reset(mode + 1);
          break;
        }
      }
      break;
    }


    case modeTSPLIT2: {
      lcd.setCursor(0,0);
      lcd.print("Test Hi Contrast");
      lcd.setCursor(0,1);
      lcd.print("Time: ");
      lcd.print(timer);
      lcd_key = read_LCD_buttons();
      if (lighton && ! beeperon){
        t.after(1000, beep);
        beeperon=true;
      }
      switch (lcd_key){
        case btnSELECT: {
          if (lighton){
            lcd.setCursor(10,1);
            lcd.print("OFF    ");
            lightset(0,0,0);
            lighton=false;
          }else{
            if (timer > 0){
              lcd.setCursor(10,1);
              lcd.print("Ready?");
              tone(13, NOTE_C4,200);
              delay(1000);
              tone(13, NOTE_C4,200);
              lcd.setCursor(10,1);
              lcd.print("G ON    ");
              lightset(0,200,0);
              delay(timer * 1000);
              lightset(200,0,0);
              lcd.setCursor(10,1);
              lcd.print("G OFF    ");
              delay(1000);
              lcd.setCursor(10,1);
              lcd.print("B ON    ");
              lightset(0,0,200);
              lighton=true;
              beeperon=false;
            }
          }
          break;
        }
        case btnLEFT: {
          timer = timer + 5;
          lcd.setCursor(0,1);
          lcd.print("Time:     ");
          lcd.setCursor(6,1);
          lcd.print(timer);
          break;
        }
        case btnRIGHT: {
          timer = timer + 1 ;
          lcd.setCursor(0,1);
          lcd.print("Time:     ");
          lcd.setCursor(6,1);
          lcd.print(timer);
          break;
        }
        case btnUP: {
          reset(mode - 1);
          break;
        } 
        case btnDOWN: {
          reset(0);
          break;
        } 
      }
      break;
    }

  }

}
