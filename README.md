# An arduino controller for a RGB darkroom enlarger/timer

This program allows to control the color, intensity and timing of an RGB LED,
to be used as the light source for a photographic darkroom enlarger.

# Hardware

The different functions (safelight, test strips, print, split grade, etc) can
be selected and configured using an arduino LCD KeyPad Shield.
http://www.dx.com/p/lcd-keypad-shield-for-arduino-duemilanove-lcd-1602-118059
Remember to cover the LCD with some translucent red tape!

Additionally, depending on the LED you choose you may need a separate LED driver
(I used http://www.dx.com/p/full-color-rgb-led-strip-driver-module-for-arduino-blue-black-314667#.Wu-inoApDRE)

# Building and loading

    sudo apt-get install arduino-mk
    make
    make upload
